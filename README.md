# pypidemo

Demo how to use python lib with gitlab repository.

* Every commit to main branch will publish a pkg-[version].dev[number]+[commit]. 
  * setuptools_scm is smart enough to figure out that [version] should be the latest tag +1
* If a tag is pushed it will publish a pkg-[version] 

See: https://gitlab.com/varokas-demo/pypidemo/-/packages

## Setup to use gitlab private pypi

1. All pypi projects should be in the same group (eg. https://gitlab.com/varokas-demo). Take note of the group id.
2. Create a personal access token -- https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token
   * need an `api` permission
3. edit `~/.config/pip/pip.conf` (note the project id)
```
[global]
extra-index-url = https://<token_name>:<token>@gitlab.com/api/v4/groups/<group_id>/-/packages/pypi/simple
```

## Use the library in another project
```
$ mkdir new_project
$ python -m venv .venv
$ source .venv/bin/activate

$ pip install pypidemo --force --pre

Looking in indexes: https://pypi.org/simple, https://pypi:****@gitlab.com/api/v4/groups/15942736/-/packages/pypi/simple
Collecting pypidemo
  Downloading https://gitlab.com/api/v4/groups/15942736/-/packages/pypi/files/1a011d98dd13f5b7964a4649582a223e78a687c8ffe6fc90e442d30e8a3f7269/pypidemo-0.1.dev19%2Bg5e714fa-py3-none-any.whl (1.5 kB)
Installing collected packages: pypidemo
  Attempting uninstall: pypidemo
    Found existing installation: pypidemo 0.1.dev19+g5e714fa
    Uninstalling pypidemo-0.1.dev19+g5e714fa:
      Successfully uninstalled pypidemo-0.1.dev19+g5e714fa
Successfully installed pypidemo-0.1.dev19+g5e714fa
```

## Create a release build
```
$ git tag 0.1 # Tag the current branch as 0.1
$ git push --tags
```

A new build will be made with 0.1 as release version. The next main push will start from 0.2

## Reference
  * https://docs.gitlab.com/ee/user/packages/pypi_repository/
